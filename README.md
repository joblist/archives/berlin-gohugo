# Front end for joblistberlin.com

Accessible on [joblistberlin.com](https://joblistberlin.com).

## Update a company

All companies are located in `./content/companies`.

A company front-matter could look like this:

```
+++
slug = "example-company"
title = "Example Company"
body = "This is what we do"
tags = ["gree", "transition", "social", "justice"]
job_board_url = "https://exmple.com"
job_board_provider = "greenhouse"
job_board_hostname = "example"
address = "Berlinerstr 101, 10115 Berlin, Germany"
latitude = 52.5303055
longitude = 13.3842364
linkedin_url = "https://www.linkedin.com/company/example"
twitter_url = "https://twitter.com/example"
facebook_url = "https://www.facebook.com/example"
created_at = "2017-06-28T20:32:41.806Z"
updated_at = "2020-04-04T17:21:41.806Z"
is_approved = true
draft = false
+++

```

## Development 

Tou will need [gohugo.io](https://gohugo.io/documentation)

To run the development server: `hugo server`


