const fs = require('fs')
const readline = require('readline')
const fetch = require('isomorphic-fetch')
const cheerio = require('cheerio')

const readInterface = readline.createInterface({
  input: fs.createReadStream('./input.txt'),
  output: process.stdout,
  console: false
})


readInterface.on('line', line => handleCompanyUrl(line))

const handleCompanyUrl = async (url) => {
	const links = await findCompanyLinks(url)
	const company = {
		url,
		links
	}
	console.log(company)
}

const findCompanyLinks = async (url) => {
	let page
	try {
		page = await fetchCompany(url)
	} catch (e) {
		console.error('error fetching url', e, url)
		return
	}

	const $ = cheerio.load(page)

	const $links = $('a')

	let matchedLinks = {
		jobs: [],
		instagram: [],
		facebook: [],
		linkedin: [],
		twitter: []
	}

	$links.each((index, item) => {
		const baseUrl = item.attribs.href
		if (typeof baseUrl !== 'string') return

		const matches = [
			findCareerUrl(baseUrl),
			linkedinUrl(baseUrl),
			twitterUrl(baseUrl),
			findFacebookUrl(baseUrl),
			findInstagramUrl(baseUrl)
		].filter(item => item)

		if (!matches.length) return

		matches.forEach(match => {
			if (!matchedLinks[match.id].includes(match.url)) {
				matchedLinks[match.id].push(match.url)
			}
		})
	})

	return matchedLinks
}

const fetchCompany = url => {
	return fetch(url).then(res => {
		return res.text()
	})
}

const findCareerUrl = url => {
	if (
		url.indexOf('work') < 0
		&& url.indexOf('jobs') < 0
		&& url.indexOf('career') < 0
	) return null

	return {
		id: 'jobs',
		url: url
	}
}

const findInstagramUrl = url => {
	if (url.indexOf('instagram') < 0) return null
	return {
		id: 'instagram',
		url: url
	}
}

const findFacebookUrl = url => {
	if (url.indexOf('facebook') < 0) return null
	return {
		id: 'facebook',
		url: url
	}
}

const linkedinUrl = url => {
	if (url.indexOf('linkedin') < 0) return null
	return {
		id: 'linkedin',
		url: url
	}
}

const twitterUrl = url => {
	if (url.indexOf('twitter') < 0) return null
	return {
		id: 'twitter',
		url: url
	}
}
