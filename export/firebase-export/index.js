var fs = require('fs')

const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

const config = {
  "projectId": "joblistberlin",
  "apiRootUrl": "https://joblistberlin.firebaseio.com",
  "apiKey": "AIzaSyACEEOsMA6xpUV4uzgbQtK8T7GNLOdQRfQ",
  "databaseURL": "joblistberlin.firebaseio.com",
  "authDomain": "joblistberlin.firebaseapp.com",
  "storageBucket": "joblistberlin.appspot.com"
}

firebase.initializeApp(config);
const db = firebase.firestore();


const COMPANIES_DIR = './companies'

// format a string to be used in a url
const slugify = (string = '') => {
  return string
		.toLowerCase()
		.trim()
		.replace(/[`~!@#$%^&*()_|+=?;:'",.<>{}[\]\\/]/gi, '')
		.replace(/\s+/gi, '-')
}

// find all hashtags in a string
const findTags = searchText => {
  // https://regexr.com/46r2p
  var regexp = /(?:\B#)(\w|-?)+\b/g
  let result = searchText.match(regexp);
  if (result) {
		return result.map(item => item.replace('#',''));
  } else {
		return false;
  }
}

const serialize = (data) => {
  return data.map(item => {
		item.slug = slugify(item.title)

		item.jobBoardUrl = item.url
		delete item.url

		if (item.position) {
			item.latitude = item.position._lat
			item.longitude = item.position._long
			delete item.position
		}

		if (item.createdAt) {
			item.createdAt = new Date(item.createdAt.toDate())
		}
		if (item.updatedAt) {
			item.updatedAt = new Date(item.updatedAt.toDate())
		}

		return item
  })
}

const getCompanies = async () => {
  let data = []
  try {
		await db.collection('links')
						.get()
						.then((snapshot) => {
							snapshot.forEach((doc) => {
								data.push(doc.data())
							});
						})
						.catch((err) => {
							console.log('Error getting documents', err);
						});
  } catch (e) {
		console.error('error fetching api')
  }
  return serialize(data)
}

const writeDataToFiles = (data) => {
  for (let i = 0; i < data.length; i++) {
		const companyPath = `${COMPANIES_DIR}/${data[i].slug}.md`
		const companyS = JSON.stringify(data[i])
		const companyData = companyS

		fs.writeFileSync(companyPath, companyData)
		console.log('wrote:', companyPath)
  }
}

const main = async () => {
  const data = await getCompanies()
  if (!data) {
		console.log('Api has no companies companies')
		return
  }

  if (!fs.existsSync(COMPANIES_DIR)) {
		fs.mkdirSync(COMPANIES_DIR)
  }

	await writeDataToFiles(data)
  try {
  } catch (e) {
		console.log('Error writting files')
  }
}

main()
