+++
title = "Info"
+++

## About

**Job List Berlin** is an open website for job offers and companies specificaly hiring in the city of [Berlin](https://en.wikipedia.org/wiki/Berlin), Germany (Europe).

Feel free to add missing or update existing information.

Don't hesitate to get in touch [gitlab/joblist/berlin](https://gitlab.com/joblist/berlin).

## Terms of & list of services

Please, read and agree with the following licenses and terms for our
systems and softwares, and the one they depend on.

Internal:
- code license: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) or [aGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).
- data license: [odbl](https://opendatacommons.org/licenses/odbl/)
- content license: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- content & code & data attributions: [JobListCity contributors](https://joblist.city)

Dependencies:
- [OSM](https://www.openstreetmap.org/copyright) (Open Street Map)
