+++
body = "Company providing #perfume for #corporate gifts"
created_at = "2018-02-02T10:46:30.180Z"
is_approved = true
job_board_url = "https://www.frau-tonis-parfum.com/karriere-jobs/"
latitude = 52.507474
longitude = 13.388228
slug = "frau-tonis-parfum"
tags = ["perfume", "corporate"]
title = "Frau Tonis Parfum"
updated_at = "2019-06-16T10:36:08.537Z"

+++
