+++
slug = "point-nine-capital"
title = "Point Nine Capital"
body = "Point Nine Capital is an #early-stage #venture #capital firm primarily focused on SaaS and online marketplaces"
tags = ["early-stage", "venture", "capital"]
job_board_url = "https://pointninecapital.recruitee.com"
address = "Point Nine Capital, Chausseestraße 19, 10115 Berlin, Germany"
latitude = 52.5303059
longitude = 13.3842365
linkedin_url = "https://www.linkedin.com/company/point-nine-capital"
twitter_url = "https://twitter.com/PointNineCap"
facebook_url = "https://www.facebook.com/PointNineCap"
created_at = "2017-06-28T20:32:41.806Z"
updated_at = "2020-04-04T17:21:41.806Z"
is_approved = true
draft = false
+++
