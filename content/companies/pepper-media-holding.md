+++
body = "Shopping platform #startup ; #marketplace"
created_at = "2019-04-11T09:21:23.163Z"
is_approved = true
job_board_url = "https://join.pepper.com/"
latitude = 52.528782
longitude = 13.39633
slug = "pepper-media-holding"
tags = ["startup", "marketplace"]
title = "Pepper Media Holding "
updated_at = "2019-06-16T10:36:09.740Z"

+++
