+++
body = "Smunch is the perfect way to bring teams together over delicious food. We offer lunches delivered daily to the office so companies can strengthen their employer branding and retain employees. #startup #delivery"
created_at = "2019-09-10T09:04:11.438Z"
is_approved = true
job_board_url = "https://www.smunch.co/en/career"
latitude = 52.5048705
longitude = 13.3663816
slug = "smunch"
tags = ["startup", "delivery"]
title = "Smunch"

+++
