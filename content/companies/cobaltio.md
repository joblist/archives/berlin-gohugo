+++
body = "#it-security #startup; #it #software"
created_at = "2018-10-05T20:11:40.451Z"
is_approved = true
job_board_url = "https://cobalt-labs.workable.com/"
latitude = 52.5111932
longitude = 13.3911237
slug = "cobaltio"
tags = ["it-security", "startup", "it", "software"]
title = "Cobalt.io"
updated_at = "2019-06-16T10:36:08.540Z"

+++
