+++
job_board_url = "https://jobs.lever.co/limebike"
title = "Lime"
body = "Lime is reinventing multimodal transportation and helping people get where they need to go quicker, easier and more affordably than ever before."
tags = ["startup", "mobility", "transportation"]
city="berlin"
country="germany"
company_url = "https://www.li.me"
slug = "lime-bike"
linkedin_url = "https://www.linkedin.com/company/limebike"
twitter_url = "https://twitter.com/limebike"
instagram_url = "https://www.instagram.com/lime"
facebook_url = "https://www.facebook.com/limebike"
created_at = "2020-09-08T12:45:18.626Z"
updated_at = "2020-09-08T12:45:18.626Z"
is_approved = true
draft = false
+++
