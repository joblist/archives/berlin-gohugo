+++
job_board_url = "https://jobs.lever.co/beyondmeat"
title = "Beyond Meat"
body = "Producer of plant-based meat substitutes."
tags = ["food", "startup"]
city="berlin"
country="germany"
company_url = "https://www.beyondmeat.com"
slug = "beyond-meat"
linkedin_url = "https://www.linkedin.com/company/beyond-meat/"
twitter_url = "https://twitter.com/beyondmeat"
instagram_url = "https://www.instagram.com/beyondmeat"
facebook_url = "https://www.facebook.com/beyondmeat"
created_at = "2020-09-08T13:00:18.626Z"
updated_at = "2020-09-08T13:00:18.626Z"
is_approved = true
draft = false
+++
