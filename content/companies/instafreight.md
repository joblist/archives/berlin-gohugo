+++
body = "#logistics #startup #organization"
created_at = "2018-06-21T18:13:15.449Z"
is_approved = true
job_board_url = "https://www.instafreight.de/en/jobs"
latitude = 52.501065
longitude = 13.4206559
slug = "instafreight"
tags = ["logistics", "startup", "organization"]
title = "InstaFreight"
updated_at = "2019-06-16T10:36:09.735Z"

+++
