+++
job_board_url = "https://www.labforward.io/careers"
job_board_provider = "smartrecruiters"
job_board_hostname = "Labforward"
title = "Labforward"
slug = "labforward"
body = "Software solutions to streamline the daily work of scientific in the lab. The platforms that we build are meant to simplify workflows and scientific documentation. Our collaborative, easy to use and make scientific data more reliable. We are building the operating system of the lab, connecting data, devices and researchers."
tags = ["startup", "science", "documentation", "platform", "laboratory"]
address = "Elsenstraße 106"
postal_code = "12435"
city="berlin"
country="germany"
latitude = 52.489769
longitude = 13.455388
company_url = "https://www.labforward.io"
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
