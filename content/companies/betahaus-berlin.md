+++
body = "#coworking space #startup #freelance"
created_at = "2017-06-28T20:22:43.838Z"
is_approved = true
job_board_url = "http://www.betahaus.com/berlin/"
latitude = 52.4956458
longitude = 13.4205617
slug = "betahaus-berlin"
tags = ["coworking", "startup", "freelance"]
title = "Betahaus Berlin"
updated_at = "2019-06-16T10:36:09.741Z"

+++
