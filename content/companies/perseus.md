+++
body = "Cyber security company. #startup"
created_at = "2020-04-04T12:50:09.626Z"
is_approved = true
job_board_url = "https://perseustechnologiesgmbh.recruitee.com"
latitude = 52.508453
longitude = 13.329163 
address = "Hardenbergstrasse 32"
postal_code = "10623"
city="berlin"
country="germany"
slug = "perseus-technologies-gmbh"
tags = ["security", "cyber-security", "software", "hardware", "startup"]
title = "Perseus Technologies Gmbh"
updated_at = "2020-04-04T12:50:09.626Z"
+++
