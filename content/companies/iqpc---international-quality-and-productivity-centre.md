+++
body = "#event and #networking organization"
created_at = "2018-02-06T08:42:16.274Z"
is_approved = true
job_board_url = "https://iqpc-2.workable.com/"
latitude = 52.5190477
longitude = 13.3887722
slug = "iqpc---international-quality-and-productivity-centre"
tags = ["event", "networking"]
title = "IQPC - International Quality and Productivity Centre"
updated_at = "2019-06-16T10:36:09.731Z"

+++
