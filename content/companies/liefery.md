+++
job_board_url = "https://liefery-jobs.personio.de"
title = "Liefery"
body = "Logistics startup, building an online marketplace for shops and couriers that enables the next generation of fast and customer friendly delivery experiences. Liefery is providing customized delivery features like same-day-delivery or delivery in selectable time-window to some of the biggest online shops."
tags = ["startup", "logistics"]
address = "Brunnenstraße 128"
postal_code = "13355"
city="berlin"
country="germany"
company_url = "https://www.liefery.com"
slug = "liefery-gmbh"
latitude = 52.539526
longitude = 13.394650
linkedin_url = "https://www.linkedin.com/company/5324005"
twitter_url = "https://twitter.com/liefery_de"
instagram_url = "https://www.instagram.com/liefery_inside"
facebook_url = "https://www.facebook.com/same.day.delivery"
created_at = ""
updated_at = "2020-09-08T20:42:18.626Z"
is_approved = true
draft = false
+++
