+++
job_board_url = "https://partum-gmbh-jobs.personio.de"
title = "Construyo"
body = "Offers a complete project management service that connects the architecture, engineering and construction (AEC) industry."
tags = ["startup", "architecture", "construction"]
address = "Borsigstraße 8"
postal_code = "10115"
city="berlin"
country="germany"
company_url = "https://www.construyo.de"
slug = "construyo-partum-gmbh"
latitude = 52.529284
longitude = 13.390135
linkedin_url = "https://www.linkedin.com/company/wirsindconstruyo"
twitter_url = ""
instagram_url = "https://www.instagram.com/wirsindconstruyo"
facebook_url = "https://www.facebook.com/wirsindConstruyo"
created_at = "2020-09-08T16:27:18.626Z"
updated_at = "2020-09-08T16:27:18.626Z"
is_approved = true
draft = false
+++
