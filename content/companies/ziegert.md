+++
job_board_url = "https://ziegert-jobs.personio.de"
title = "Ziegert"
body = "Real estate consultancy in Berlin."
tags = ["consulting", "real-estate"]
address = "Zimmerstr. 16"
postal_code = "10969"
city="berlin"
country="germany"
company_url = "https://www.ziegert-immobilien.de"
slug = "ziegert-immobilien"
latitude = 52.507610
longitude = 13.389152
linkedin_url = "https://www.linkedin.com/company/ziegert-bank--&-real-estate-consulting-gmbh"
twitter_url = ""
instagram_url = "https://www.instagram.com/ziegert_immobilien"
facebook_url = "https://www.facebook.com/ZiegertBankundImmobilienconsulting/"
created_at = "2020-09-08T16:53:09.626Z"
updated_at = "2020-09-08T16:53:09.626Z"
is_approved = true
draft = false
+++
