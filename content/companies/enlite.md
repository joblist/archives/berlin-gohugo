+++
body = "#consulting for #energy transition"
created_at = "2018-12-21T10:40:46.156Z"
is_approved = true
job_board_url = "https://enlite.de/en/career/"
latitude = 52.427439
longitude = 13.3139077
slug = "enlite"
tags = ["consulting", "energy"]
title = "Enlite"
updated_at = "2019-06-16T10:36:08.536Z"

+++
