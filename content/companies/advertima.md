+++
body = "Advertima uses #computer-vision and #machine-learning to interpret the visual appearance, walking paths, and body language of people in real-time. This data enables different real-time applications like fully automated customer interactions through screens, provides customer insights and analytics for physical spaces, and empowers existing retail stores to become cashier-less and autonomous."
created_at = "2020-02-29T12:58:55.000Z"
is_approved = true
job_board_url = "https://www.advertima.com/company/career-opportunity"
latitude = 52.52995
longitude = 13.3901214
slug = "advertima"
tags = ["computer-vision", "machine-learning", "spaces", "retail"]
title = "Advertima"

+++
