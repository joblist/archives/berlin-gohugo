+++
job_board_url = "https://verimi-jobs.personio.de"
title = "Verimi"
body = "Solutions for digital identity management."
tags = ["startup", "digital-identity"]
address = "Oranienstr. 91"
postal_code = "10969"
city="berlin"
country="germany"
company_url = "https://verimi.de"
slug = "verimi-gmbh"
latitude = 52.503221
longitude = 13.412471
linkedin_url = "https://linkedin.com/company/verimi"
twitter_url = "https://twitter.com/VERIMI_Now"
instagram_url = ""
facebook_url = ""
created_at = ""
updated_at = "2020-09-08T20:33:18.626Z"
is_approved = true
draft = false
+++
