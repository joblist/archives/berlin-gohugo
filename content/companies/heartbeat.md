+++
job_board_url = "https://heartbeat-med-jobs.personio.de"
title = "Heartbeat"
body = "Heartbeat medical is an expert in Patient Reported Outcome Measures (PROMs) and offers a software to optimize workflows, save time in the everyday clinical routine and automate the tracking of PROMs."
tags = ["startup", "medical"]
address = "Greifswalder Str. 212"
postal_code = "10405"
city="berlin"
country="germany"
company_url = "https://heartbeat-med.com"
slug = "heartbeat-med"
latitude =  52.538271
longitude = 13.435621
linkedin_url = ""
twitter_url = ""
instagram_url = ""
facebook_url = ""
created_at = "2020-09-08T16:16:18.626Z"
updated_at = "2020-09-08T16:16:18.626Z"
is_approved = true
draft = false
+++
