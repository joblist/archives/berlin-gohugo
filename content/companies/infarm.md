+++
body = "Infarm is an #urban #farming services company that develops farming tech for grocery stores, restaurants, and #local #distribution centres #food"
created_at = "2017-06-28T20:39:32.559Z"
is_approved = true
job_board_url = "https://boards.greenhouse.io/infarm"
latitude = 52.456677
longitude = 13.3927142
slug = "infarm"
tags = ["urban", "farming", "local", "distribution", "food"]
title = "Infarm"
updated_at = "2019-06-16T10:36:08.521Z"

+++
