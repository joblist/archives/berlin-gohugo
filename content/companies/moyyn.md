+++
job_board_url = "https://moyyn.com/open-positions"
title = "Moyyn"
body = "Moyyn is an engineering and software recruitment agency for highly skilled professionals, currently also having marketing/sales roles, executive, VP positions and more. We matchmake companies with verified and experienced candidates quickly, thereby reducing their sourcing time and costs."
tags = ["recruitment", "agency"]
address = "Startup Incubator Berlin, Rohrdamm 88"
postal_code = "13629"
city="berlin"
country="germany"
company_url = "https://www.moyyn.com/"
is_approved = true
created_at = "2020-08-24T20:12:38.198Z"
+++
