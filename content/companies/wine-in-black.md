+++
body = "Wine in Black is a #platform allowing you to #shop #wine and #champagne #online"
created_at = "2017-06-28T20:25:57.675Z"
is_approved = true
job_board_url = "https://www.wine-in-black.de/karriere"
latitude = 52.50997
longitude = 13.389749
slug = "wine-in-black"
tags = ["platform", "shop", "wine", "champagne", "online"]
title = "Wine In Black"
updated_at = "2019-06-16T10:36:08.535Z"

+++
