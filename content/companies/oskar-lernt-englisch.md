+++
job_board_url = "https://oskarlerntenglisch.recruitee.com/"
title = "Oskar Lernt Englisch"
slug = "oskar-lernt-englisch"
body = "Oskar lernt Englisch is one of the leading providers of English language camps and courses in Germany, offering camps throughout the country since 2004, and courses in Berlin and Brandenburg since 1999."
tags = ["languages", "teacher", "youth"]
address = "Lauterstraße 21"
postal_code = "12159"
city="berlin"
country="germany"
latitude = 52.473134
longitude = 13.335566
created_at = "2020-04-04T13:54:09.626Z"
updated_at = "2020-04-04T13:54:09.626Z"
is_approved = true
draft = false
+++
