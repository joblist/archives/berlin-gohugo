+++
body = "Visual Meta is a top reference for connecting #online shops and #consumers. We create reach for the #shops and orientation for the consumers through our cutting-edge tools and #technologies"
created_at = "2017-06-28T20:25:47.487Z"
is_approved = true
job_board_url = "http://visual-meta.com/jobs"
latitude = 52.5237681
longitude = 13.4146475
slug = "visual-meta"
tags = ["online", "consumers", "shops", "technologies"]
title = "Visual Meta"
updated_at = "2019-06-16T10:36:09.623Z"

+++
