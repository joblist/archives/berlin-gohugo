+++
body = "Booking platform for air freight. #startup #transport #aviation"
created_at = "2018-12-21T07:23:32.382Z"
is_approved = true
job_board_url = "https://www.cargo.one/jobs"
latitude = 52.5378807
longitude = 13.4067992
slug = "cargoone"
tags = ["startup", "transport", "aviation"]
title = "Cargo.one"
updated_at = "2019-06-16T10:36:09.730Z"

+++
