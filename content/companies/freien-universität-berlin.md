+++
body = "Berlin public #university ; #culture"
created_at = "2019-05-05T06:01:41.013Z"
is_approved = true
job_board_url = "https://www.fu-berlin.de/universitaet/beruf-karriere/jobs/index.html"
slug = "freien-universität-berlin"
tags = ["university", "culture"]
title = "Freien Universität Berlin"
updated_at = "2019-06-16T10:36:09.624Z"

+++
