+++
body = "AskHelmut allows you to discover #events : #concerts, movies, art #shows, dance performances and everything on stage worth seeing in Berlin"
created_at = "2017-06-28T20:37:43.744Z"
is_approved = true
job_board_url = "http://jobs.askhelmut.com/"
latitude = 52.5134019
longitude = 13.4564664
slug = "askhelmut"
tags = ["events", "concerts", "shows"]
title = "AskHelmut"
updated_at = "2019-06-16T10:36:09.643Z"

+++
