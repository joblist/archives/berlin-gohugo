+++
body = "#travel search engine; #tech #startup"
created_at = "2017-06-28T20:22:54.024Z"
is_approved = true
job_board_url = "https://www.kayak.de/careers/all"
company_url = "https://www.kayak.de"
latitude = 52.4624745
longitude = 13.4040163
slug = "kayak"
tags = ["travel", "tech", "startup"]
title = "Kayak"
updated_at = "2019-06-16T10:36:09.743Z"

+++
