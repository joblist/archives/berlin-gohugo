+++
job_board_url = "https://www.agentur-brandung.de/karriere"
job_board_provider = "smartrecruiters"
job_board_hostname = "BrandungGmbHCoKG"
title = "Brandung GmbH"
slug = "brandung"
body = "We are brandung! The independent digital full-service agency for digital communication, eCommerce and online marketing. We provide solutions in a complete package: ranging from strategic consulting through design and technical development to maintenance."
tags = ["agency", "communication", "e-commerce", "online-marketing", "consulting", "design"]
address = "Leuschnerdamm 13"
postal_code = "10999"
city="berlin"
country="germany"
latitude = 52.504636
longitude = 13.418955
company_url = "https://www.agentur-brandung.de"
linkedin_url = "https://www.linkedin.com/company/brandung-gmbh-&-co-kg "
crunchbase_url = ""
twitter_url = "https://twitter.com/brandung"
facebook_url = "https://www.facebook.com/brandung"
instagram_url = "https://www.instagram.com/agentur_brandung "
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
