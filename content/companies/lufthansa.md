+++
body = "Lufthansa, is the largest German airline and, when combined with its subsidiaries, also the largest airline in Europe both in terms of fleet size and passengers carried during 2017. #aviation"
created_at = "2018-02-09T13:59:18.227Z"
is_approved = true
job_board_url = "https://career.be-lufthansa.com/indexhttps://career.be-lufthansa.com/index.php?&search_criterion_keyword=berlin.php?&search_criterion_keyword=berlin"
slug = "lufthansa"
tags = ["aviation"]
title = "Lufthansa"
updated_at = "2019-06-16T10:36:09.730Z"

+++
