+++
body = "Funding #marketplace where entrepreneurs in search of funding meet investors in search of a simple #investment opportunity"
created_at = "2018-11-20T16:23:35.353Z"
is_approved = true
job_board_url = "https://kapilendo-ag-jobs.personio.de/"
latitude = 52.501652
longitude = 13.331367
slug = "kapilendo"
tags = ["marketplace", "investment"]
title = "Kapilendo"
updated_at = "2019-06-16T10:36:08.533Z"

+++
