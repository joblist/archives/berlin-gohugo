+++
body = "Airbus is an international pioneer in the aerospace industry. #aerospace #aviation #engineering"
created_at = "2018-02-09T14:14:34.405Z"
is_approved = true
job_board_url = "https://company.airbus.com/careers/jobs-and-applications/search-for-vacancies.html?queryStr=&city=Berlin&country=de"
slug = "airbus"
tags = ["aerospace", "aviation", "engineering"]
title = "Airbus"
updated_at = "2019-06-16T10:36:09.626Z"

+++
