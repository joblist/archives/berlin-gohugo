+++
body = "Humana is a #retail #second-hand brand with #physical #shop in germany."
created_at = "2017-06-28T20:37:35.095Z"
is_approved = true
job_board_url = "http://www.humana-second-hand.de/mode/jobs.html"
slug = "humana"
tags = ["retail", "second-hand", "physical", "shop"]
title = "Humana"
updated_at = "2019-06-16T10:36:08.530Z"

+++
