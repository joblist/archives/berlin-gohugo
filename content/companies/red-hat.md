+++
body = "#open-source #software and #it solutions; #multinational"
created_at = "2017-06-28T20:34:34.972Z"
is_approved = true
job_board_url = "http://jobs.redhat.com/job-search-results/?technology=-1&marketing=-1&sales=-1&corporate=-1&country=56&city=465004&remote=-1&keywords=Keywords"
latitude = 52.49228
longitude = 13.45742
slug = "red-hat"
tags = ["open-source", "software", "it", "multinational"]
title = "Red Hat"
updated_at = "2019-06-16T10:36:09.735Z"

+++
