+++
job_board_url = "https://planetly-jobs.personio.de"
title = "Planetly"
body = "Platform for analyzing, reducing and offsetting CO2 in corporations across Europe."
tags = ["software", "startup", "climate"]
address = "Gormannstraße 12"
postal_code = "10119"
city="berlin"
country="germany"
company_url = "https://www.planetly.org/"
slug = "planetly"
latitude =  52.528307
longitude = 13.405662
linkedin_url = "https://www.linkedin.com/company/planetly/"
twitter_url = "https://twitter.com/planetly_berlin"
instagram_url = "https://www.instagram.com/_planetly/"
facebook_url = "https://www.facebook.com/planetly"
created_at = "2020-09-08T12:50:18.626Z"
updated_at = "2020-09-08T12:50:18.626Z"
is_approved = true
draft = false
+++
