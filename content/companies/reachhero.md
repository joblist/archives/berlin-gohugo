+++
body = "ReachHero is one of Germany’s largest platform for #influencer #marketing. Builds #technology to bring #brands, influencers and #consumers closer together"
created_at = "2018-02-01T15:55:09.800Z"
is_approved = true
job_board_url = "https://www.reachhero.de/jobs"
latitude = 52.5269568
longitude = 13.4168964
slug = "reachhero"
tags = ["influencer", "marketing", "technology", "brands", "consumers"]
title = "ReachHero"
updated_at = "2019-06-16T10:36:08.541Z"

+++
