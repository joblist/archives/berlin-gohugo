+++
body = "#digital #design #agency"
created_at = "2019-01-24T08:42:12.489Z"
is_approved = true
job_board_url = "https://goodpatch.com/studios/berlin?lng=en"
latitude = 52.499892
longitude = 13.425056
slug = "goodpatch"
tags = ["digital", "design", "agency"]
title = "Goodpatch"
updated_at = "2019-06-16T10:36:08.534Z"

+++
