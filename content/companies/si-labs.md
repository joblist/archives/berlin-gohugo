+++
body = "Service #innovation and #business transformation"
created_at = "2020-02-29T16:32:31.000Z"
is_approved = true
job_board_url = "https://si-labs-jobs.personio.de/"
latitude = 52.504257981230616
longitude = 13.41461777687073
slug = "si-labs"
tags = ["innovation", "business"]
title = "SI Labs"

+++
