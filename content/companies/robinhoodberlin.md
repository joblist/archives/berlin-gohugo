+++
body = "#startup building collaborative and collective homes as small pods in Berlin. #housing #architecture #coworking space"
created_at = "2019-02-01T07:37:33.650Z"
is_approved = true
job_board_url = "https://www.robinhood.berlin/jobs"
slug = "robinhoodberlin"
tags = ["startup", "housing", "architecture", "coworking"]
title = "robinhood.berlin"
updated_at = "2019-06-16T10:36:08.540Z"

+++
