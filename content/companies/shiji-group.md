+++
job_board_url = "https://www.shijigroup.com/careers"
job_board_provider = "smartrecruiters"
job_board_hostname = "ShijiGroup"
title = "Shiji Group"
slug = "shiji-group"
body = "Over the past 20 years, Shiji Group has become a major software supplier in the hospitality and retail industries, developing worldwide leading products and technologies."
tags = ["software", "hospitality", "retail", "industry", "technology"]
address = "Tempelhofer Ufer 1"
postal_code = "10963"
city="berlin"
country="germany"
latitude = 52.497334
longitude = 13.390323
company_url = "https://www.shijigroup.com"
googleMapsUrl = "https://g.page/shiji-europe-berlin"
linkedin_url = "https://www.linkedin.com/company/shijigroup"
twitter_url = "https://twitter.com/ShijiGroup"
facebook_url = "https://business.facebook.com/ShijiGroup"
created_at = "2020-07-04T22:47:18.626Z"
updated_at = "2020-07-04T22:47:18.626Z"
is_approved = true
draft = false
+++
