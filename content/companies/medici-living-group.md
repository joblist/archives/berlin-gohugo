+++
body = "Co-living provider. #tech #real-estate"
created_at = "2018-02-14T22:29:23.167Z"
is_approved = true
job_board_url = "https://www.medici-living-group.com/career"
latitude = 52.49449
longitude = 13.39635
slug = "medici-living-group"
tags = ["tech", "real-estate"]
title = "Medici Living Group"
updated_at = "2019-06-16T10:36:08.539Z"

+++
