+++
job_board_url = "https://gymondo-gmbh-jobs.personio.de"
title = "Gymondo"
body = "Gymondo is a leading health and fitness app offering short and effective workouts and customized meal plans to users united by the desire to get fit and feel happy."
tags = ["startup", "healt", "fitness", "app"]
address = "Rungestr. 22-24"
postal_code = "10179"
city="berlin"
country="germany"
company_url = "https://www.gymondo.com"
slug = "gymondo-gmbh"
latitude = 52.512493
longitude = 13.418757
linkedin_url = "https://www.linkedin.com/company/gymondo-gmbh"
twitter_url = ""
instagram_url = "https://www.instagram.com/gymondo_official"
facebook_url = ""
created_at = "2020-09-08T16:23:18.626Z"
updated_at = "2020-09-08T16:23:18.626Z"
is_approved = true
draft = false
+++
