+++
job_board_url = "https://hirschen-group-jobs.personio.de"
title = "Hirschen Group"
body = "Platform for a wide variety of communication and consulting agency brands that offer creative advice for communication, digitization and public opinion."
tags = ["agency", "communication", "consulting", "public-opinion"]
address = "Schlesische Straße 26, Aufgang C-IV"
postal_code = "10997"
city="berlin"
country="germany"
company_url = "https://hirschen-group.com"
slug = "hirschen-group"
linkedin_url = "https://www.linkedin.com/company/hirschen-group-gmbh"
twitter_url = "https://twitter.com/hirschengroup"
instagram_url = ""
facebook_url = ""
created_at = "2020-09-08T16:48:18.626Z"
updated_at = "2020-09-08T16:48:18.626Z"
is_approved = false
draft = true
+++
