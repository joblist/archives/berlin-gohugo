+++
body = "JOIN represents the future of #recruiting and is in an exceptional position to conquer the #HR Tech world by storm. You are the only missing piece in the puzzle! #startup"
created_at = "2020-03-01T16:23:12.377Z"
is_approved = true
job_board_url = "https://join.join.com"
latitude = 52.538563
longitude = 13.413107
slug = "join"
tags = ["recruiting", "HR", "startup"]
title = "Join"

+++
