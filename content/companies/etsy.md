+++
body = "Etsy platform enables you to find the perfect #handmade #gift, #vintage & on-trend #clothes, unique #jewelry, and more"
created_at = "2017-06-28T20:24:51.327Z"
is_approved = true
job_board_url = "https://www.etsy.com/de/careers/"
slug = "etsy"
tags = ["handmade", "gift", "vintage", "clothes", "jewelry"]
title = "Etsy"
updated_at = "2019-06-16T10:36:08.540Z"

+++
