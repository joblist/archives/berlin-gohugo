+++
job_board_url = "https://roobeo-jobs.personio.de"
title = "Roobeo"
body = "Platform for digitizing material procurement - processes made easy for craft businesses, retail and industry."
tags = ["startup"]
address = "Kleine Präsidentenstraße 1"
postal_code = "10178"
city="berlin"
country="germany"
company_url = "https://www.roobeo.com"
slug = "roobeo-gmbh"
latitude = 52.522636
longitude = 13.400513
linkedin_url = "https://www.linkedin.com/company/roobeo"
twitter_url = "https://twitter.com/ROOBEO_GmbH"
instagram_url = ""
facebook_url = "https://www.facebook.com/ROOBEOgmbh"
created_at = "2020-09-08T16:35:18.626Z"
updated_at = "2020-09-08T16:35:18.626Z"
is_approved = true
draft = false
+++
