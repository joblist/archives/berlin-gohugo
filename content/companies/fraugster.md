+++
body = "Fraugster is a payment security company with the goal of eliminating fraud and increasing our customers’ profits. Over the last few years we’ve invented an artificial intelligence technology that combines human-like accuracy with machine scalability. This ability lets us foresee fraudulent attacks before they actually happen and gives us a distinct competitive advantage over every other player in the payment security space. #security #it #startup"
created_at = "2020-02-25T07:48:07.548Z"
is_approved = true
job_board_url = "https://fraugster.com/jobs/"
latitude = 52.50640031375411
longitude = 13.393020629882814
slug = "fraugster"
tags = ["security", "it", "startup"]
title = "Fraugster"

+++
