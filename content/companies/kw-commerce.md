+++
body = "#startup #marketplace #e-commerce"
created_at = "2018-02-08T09:35:16.719Z"
is_approved = true
job_board_url = "https://jobs.kw-commerce.de/Jobs"
latitude = 52.5148845
longitude = 13.3969957
slug = "kw-commerce"
tags = ["startup", "marketplace", "e-commerce"]
title = "KW-Commerce"
updated_at = "2019-06-16T10:36:09.730Z"

+++
