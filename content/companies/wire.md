+++
body = "Wire is a #secure #messaging solution for everyone"
created_at = "2017-06-28T20:33:14.687Z"
is_approved = true
job_board_url = "https://www.wire.com/jobs/"
latitude = 52.5026393
longitude = 13.4408648
slug = "wire"
tags = ["secure", "messaging"]
title = "Wire"
updated_at = "2019-06-16T10:36:09.740Z"

+++
