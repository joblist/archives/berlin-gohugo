+++
body = "BMW is a German multinational company which currently produces luxury automobiles and motorcycles #automobile"
created_at = "2018-02-01T20:32:18.959Z"
is_approved = true
job_board_url = "https://www.bmwgroup.com/de/karriere.html#location=DE/Berlin"
slug = "bmw"
tags = ["automobile"]
title = "BMW"
updated_at = "2019-06-16T10:36:08.530Z"

+++
