+++
body = "Infopark is a #digital #agency accompanying its customers in the digital transformation and creation custom solutions"
created_at = "2017-06-28T20:40:05.744Z"
is_approved = true
job_board_url = "https://infopark.com/de/unternehmen/karriere#jobs"
latitude = 52.43269
longitude = 13.37386
slug = "infopark"
tags = ["digital", "agency"]
title = "Infopark"
updated_at = "2019-06-16T10:36:08.531Z"

+++
