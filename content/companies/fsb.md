+++
body = "FSB’s Innovation Lab in Berlin fuses #startup verve with the savvy of an established brand #enterprise. We develop future-driven systems for smart buildings – including both the #hardware to the #software. #smart-building #construction #space"
created_at = "2020-02-18T15:50:28.296Z"
is_approved = true
job_board_url = "https://www.fsb.de/en/company/dac/"
latitude = 52.568976
longitude = 13.312013
slug = "fsb"
tags = ["startup", "enterprise", "hardware", "software", "smart-building", "construction", "spaces"]
title = "FSB"

+++
