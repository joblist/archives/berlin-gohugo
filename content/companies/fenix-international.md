+++
job_board_url = "https://boards.greenhouse.io/fenixinternational"
title = "Fenix International"
body = "Fenix International produces solar panel solutions."
tags = ["energy", "solar"]
city="berlin"
country="germany"
company_url = "https://www.fenixintl.com"
slug = "fenix-international"
linkedin_url = "https://www.linkedin.com/company/fenix-international"
twitter_url = "https://twitter.com/fenixintl"
facebook_url = "https://www.facebook.com/fenixintl"
created_at = "2020-09-08T12:35:18.626Z"
updated_at = "2020-09-08T12:35:18.626Z"
is_approved = true
draft = false
+++
