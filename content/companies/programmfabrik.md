+++
body = "Programmfabrik is a #management #software editor aiming to improve workflows and human ressources within companies. "
created_at = "2017-06-28T20:39:14.404Z"
is_approved = true
job_board_url = "https://www.programmfabrik.de/karriere/"
latitude = 52.5340781
longitude = 13.4104639
slug = "programmfabrik"
tags = ["management", "software"]
title = "Programmfabrik"
updated_at = "2019-06-16T10:36:09.735Z"

+++
