+++
body = "Creating Positive Fashion #fashion"
created_at = "2018-02-09T06:41:20.745Z"
is_approved = true
job_board_url = "http://www.bd-i.de/people/"
latitude = 52.53145
longitude = 13.4322401
slug = "bdi-beneficial-design-institute"
tags = ["fashion"]
title = "BDI (Beneficial Design Institute)"
updated_at = "2019-06-16T10:36:09.731Z"

+++
