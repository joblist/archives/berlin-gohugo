+++
body = "#startup reshaping the future of #insurance with #ai; offers a personalized experience to customers with real-time"
created_at = "2019-01-15T14:22:17.614Z"
is_approved = true
job_board_url = "https://omnius.com/careers/#open-positions"
latitude = 52.5272379
longitude = 13.3445418
slug = "omnius"
tags = ["startup", "insurance", "ai"]
title = "omni:us"
updated_at = "2019-06-16T10:36:09.730Z"

+++
