+++
job_board_url = "https://yaslife-jobs.personio.de"
title = "YAS.life"
body = "Develops digital applications for health insurance companies, insurance companies and enterprises in the field of health management and prevention"
tags = ["startup", "applications", "health", "insurance"]
address = "Dircksenstraße 40"
postal_code = "10178"
city="berlin"
country="germany"
company_url = "https://yas.life"
slug = "yas-life"
latitude = 52.519956
longitude = 13.414060
linkedin_url = "https://de.linkedin.com/company/yas.life---magnum-est-digital-health-gmbh"
twitter_url = ""
instagram_url = "https://www.instagram.com/yas.healthapp"
facebook_url = "https://www.facebook.com/yas.healthapp"
created_at = "2020-09-08T20:38:18.626Z"
updated_at = "2020-09-08T20:38:18.626Z"
is_approved = true
draft = false
+++
