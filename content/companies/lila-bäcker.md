+++
body = "Lila Bäcker is a #bakery network owning several #shops in Germany"
created_at = "2017-06-28T20:40:55.222Z"
is_approved = true
job_board_url = "https://www.lila-world.com/karriere"
slug = "lila-bäcker"
tags = ["bakery", "shops"]
title = "Lila Bäcker"
updated_at = "2019-06-16T10:36:08.534Z"

+++
