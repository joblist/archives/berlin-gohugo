+++
body = "Whether hiking or biking, komoot's planning and navigation tech helps you to easily plan your next adventure. Find the best routes in your area now! We love Tech. We love Nature. We work hard to make it easy for everyone to explore the world’s most beautiful places.  Everybody at komoot is working towards this vision and we design our company to put as little as possible between you and bringing this vision to life. #nature #startup #map #navigation"
created_at = "2018-02-08T09:03:21.280Z"
is_approved = true
job_board_url = "https://www.komoot.com/jobs"
latitude = 52.3878585
longitude = 13.0583386
slug = "komoot"
tags = ["nature", "startup", "map", "navigation"]
title = "Komoot"
updated_at = "2019-06-16T10:36:09.636Z"

+++
