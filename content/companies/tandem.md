+++
body = "Tandem is a #platform to #learn #languages while connecting with other language learners"
created_at = "2017-06-28T20:39:47.570Z"
is_approved = true
job_board_url = "https://www.tandem.net/jobs"
latitude = 52.523681
longitude = 13.4214361
slug = "tandem"
tags = ["platform", "learn", "languages"]
title = "Tandem"
updated_at = "2019-06-16T10:36:09.730Z"

+++
