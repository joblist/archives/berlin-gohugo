+++
job_board_url = "https://imagine-zero.org/join"
title = "Imagine Zero"
body = "We’re all experienced professionals, who love to efficiently scale ventures. And we became engaged climate activists. With the best of these two worlds combined, we can have measurable impact at scale and thereby rethink climate activism. Of course with fun and not for profit."
tags = ["climate", "non-profit"]
address = ""
postal_code = ""
city="berlin"
country="germany"
company_url = "https://imagine-zero.org/"
slug = "imagine-zero"
linkedin_url = "https://www.linkedin.com/company/imagine-zero"
created_at = "2020-09-08T12:40:18.626Z"
updated_at = "2020-09-08T12:40:18.626Z"
is_approved = true
draft = false
+++
