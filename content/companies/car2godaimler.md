+++
body = "Car2go is an hourly #car #rental service without rental offices or return stations. Grab a car from the street and leave it anywhere in our Home Area in your City"
created_at = "2017-06-28T20:40:27.858Z"
is_approved = true
job_board_url = "https://www.car2go.com/EU/career/"
latitude = 52.5274243
longitude = 13.3670105
slug = "car2godaimler"
tags = ["car", "rental"]
title = "Car2go/Daimler"
updated_at = "2019-06-16T10:36:09.639Z"

+++
