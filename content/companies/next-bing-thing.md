+++
job_board_url = "https://nextbigthingag2.recruitee.com"
title = "Next Bing Thing"
slug = "next-bing-thing"
body = "Next Big Thing AG (NBT) is Europe's premier startup incubator for the Internet of Things and Services (IoT/IoS) and Blockchain. As a company builder and operational VC, NBT acts at the nexus of a continually growing ecosystem of corporates, founders, technologists, entrepreneurs, investors and politicians. With our know-how in IoT, we are the touchpoint for innovation in Europe - enabling companies to realize disruptive innovation with exceptional speed."
tags = ["incubator", "startup", "iot", "blockchain", "innovation"]
address = "Lohmühlenstraße 65"
postal_code = "12435"
city="berlin"
country="germany"
latitude = 52.494119
longitude = 13.446298
created_at = "2020-04-04T14:10:09.626Z"
updated_at = "2020-04-04T14:10:09.626Z"
is_approved = true
draft = false
+++
