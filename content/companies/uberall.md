+++
body = "digital location #marketing platform for local #business or #public-institution to automatically optimize their online presence; #big-data"
created_at = "2018-02-07T20:33:02.290Z"
is_approved = true
job_board_url = "https://uberall.com/en/careers?city=berlin"
latitude = 52.5164364
longitude = 13.3834469
slug = "uberall"
tags = ["marketing", "business", "public-institution", "big-data"]
title = "Uberall"
updated_at = "2019-06-16T10:36:09.631Z"

+++
