+++
body = "#ecommerce #marketplace connecting subscribers with local merchants by offering #activities, #travel, goods and services"
created_at = "2018-02-02T07:02:37.962Z"
is_approved = true
job_board_url = "https://jobs.groupon.de/search?location=berlin"
latitude = 52.5179884
longitude = 13.3536106
slug = "groupon"
tags = ["ecommerce", "marketplace", "activities", "travel"]
title = "Groupon"
updated_at = "2019-06-16T10:36:09.730Z"

+++
