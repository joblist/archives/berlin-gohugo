+++
body = "Unlimited swimming, fitness, yoga, bouldering, team sports and more with one membership all over Germany. #sport #startup"
created_at = "2018-02-24T09:22:05.414Z"
is_approved = true
job_board_url = "https://urbansportsclub-jobs.personio.de/"
latitude = 52.511399
longitude = 13.422648
slug = "urban-sports-club"
tags = ["sport", "startup"]
title = "Urban Sports Club"
updated_at = "2019-06-16T10:36:09.730Z"

+++
