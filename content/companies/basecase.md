+++
body = "BaseCase is used by the majority of top-ten pharmaceutical and medical device companies to develop and distribute customer engagement apps. #pharmaceutical #industry #startup"
created_at = "2018-02-14T08:12:20.191Z"
is_approved = true
job_board_url = "https://basecase.com/company/careers"
latitude = 52.5081627
longitude = 13.3925125
slug = "basecase"
tags = ["pharmaceutical", "industry", "startup"]
title = "BaseCase"
updated_at = "2019-06-16T10:36:09.730Z"

+++
