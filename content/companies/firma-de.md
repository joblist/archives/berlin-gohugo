+++
job_board_url = "https://firmadeen.recruitee.com"
title = "Firma"
slug = "firma"
body = "Provides a comprehensive, fast and easy company formation service that enables founders to stay business-focused instead of getting lost in legal regulations."
tags = ["startup", "formation", "founders", "business"]
address = "Poststraße 21-22"
postal_code = "10178"
city="berlin"
country="germany"
latitude = 52.516404
longitude = 13.407282
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://www.firma.de"
linkedin_url = "https://www.linkedin.com/company/firma-de"
twitter_url = "https://twitter.com/firma_de"
facebook_url = "https://www.facebook.com/durchstartenmitsystem"
instagram_url = "https://www.instagram.com/1firma"
is_approved = true
draft = false
+++
