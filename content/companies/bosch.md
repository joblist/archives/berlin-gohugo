+++
job_board_url = "https://www.bosch.com/careers"
job_board_provider = "smartrecruiters"
job_board_hostname = "BoschGroup"
title = "Bosch"
slug = "bosch"
body = "German multinational engineering and technology company "
tags = ["multinational", "technology", "engineering"]
company_url = "https://www.bosch.com"
linkedin_url = "https://www.linkedin.com/company/bosch"
twitter_url = "https://twitter.com/BoschGlobal"
facebook_url = "https://www.facebook.com/BoschGlobal"
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
