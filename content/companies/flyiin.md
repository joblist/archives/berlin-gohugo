+++
body = "#startup building user-centric #flight booking experiences based on direct connections to their partner airlines. Designs experiences for travelers to easily search, compare and buy flights and services directly with airlines. #airline #aviation"
created_at = "2019-01-10T10:07:53.750Z"
is_approved = true
job_board_url = "https://www.flyiin.com/jobs"
latitude = 52.50317
longitude = 13.372
slug = "flyiin"
tags = ["startup", "flight", "airline", "aviation"]
title = "flyiin"
updated_at = "2019-06-16T10:36:09.626Z"

+++
