+++
job_board_url = "https://coya-jobs.personio.de/"
title = "Coya"
body = "Insurance startup"
tags = ["startup", "insurance"]
address = "Ohlauer Str. 43"
postal_code = "10999"
city="berlin"
country="germany"
company_url = "https://www.coya.com"
slug = "coya"
latitude = 52.493964
longitude = 13.430118
linkedin_url = "https://www.linkedin.com/company/coya"
twitter_url = "https://twitter.com/hello_coya"
instagram_url = "https://www.instagram.com/coya"
facebook_url = "https://www.facebook.com/hellocoya"
created_at = "2020-09-08T20:27:18.626Z"
updated_at = "2020-09-08T20:27:18.626Z"
is_approved = true
draft = false
+++
