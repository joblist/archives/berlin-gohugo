+++
body = "#startup company providing a #chauffeur portal connecting people to professional chauffeurs via their #mobile application"
created_at = "2017-06-28T20:27:50.111Z"
is_approved = true
job_board_url = "https://www.blacklane.com/en/career"
latitude = 52.48483
longitude = 13.357146
slug = "blacklane"
tags = ["startup", "chauffeur", "mobile"]
title = "Blacklane"
updated_at = "2019-06-16T10:36:09.744Z"

+++
