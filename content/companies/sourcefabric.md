+++
body = "Developer of #open-source tools for #news #media. #software #tech"
created_at = "2017-11-07T07:31:06.276Z"
is_approved = true
job_board_url = "https://www.sourcefabric.org/en/home/jobs/"
latitude = 52.503296
longitude = 13.418136
slug = "sourcefabric"
tags = ["open-source", "news", "media", "software", "tech"]
title = "Sourcefabric"
updated_at = "2019-06-16T10:36:08.537Z"

+++
