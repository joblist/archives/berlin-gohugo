+++
body = "EasyJet is a British airline. #aviation #airline"
created_at = "2018-02-09T14:16:09.496Z"
is_approved = true
job_board_url = "https://easyjet.taleo.net/careersection/2/jobsearch.ftl"
slug = "easyjet"
tags = ["aviation", "airline"]
title = "EasyJet"
updated_at = "2019-06-16T10:36:09.626Z"

+++
