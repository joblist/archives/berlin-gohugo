+++
body = "German company developing, #engineering and delivering rent-to-own #solar home and business systems, appliances and services for emerging economies. #finance"
created_at = "2018-01-23T09:03:08.380Z"
is_approved = true
job_board_url = "https://mobisol-jobs.personio.de/#category-4916"
latitude = 52.500332
longitude = 13.436038
slug = "mobisol"
tags = ["engineering", "solar", "finance"]
title = "Mobisol"
updated_at = "2019-06-16T10:36:08.538Z"

+++
