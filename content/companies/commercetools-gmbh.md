+++
body = "#startup providing #ecommerce #software"
created_at = "2017-06-28T20:27:29.082Z"
is_approved = true
company_url = "http://www.commercetools.com"
job_board_url = "https://commercetools-jobs.personio.de"
latitude = 52.47359
longitude = 13.45703
slug = "commercetools-gmbh"
tags = ["startup", "ecommerce", "software"]
title = "Commercetools Gmbh"
updated_at = "2020-04-20T03:30:09.625Z"
+++
