+++
body = "Agency for FashionTech, WearableTechnologies, Internet of Things (IoT), Industry 4.0 and SmartTextiles Manufacturing. #fashion #iot #startup #agency\n"
created_at = "2018-02-18T10:43:37.704Z"
is_approved = true
job_board_url = "https://thepowerhouse.group/jobs/"
slug = "thepowerhouse"
tags = ["fashion", "iot", "startup", "agency"]
title = "ThePowerHouse"
updated_at = "2019-06-16T10:36:09.641Z"

+++
