+++
body = "#post-production, #production, #editorial, #sound-design, live events, content distribution, performance #analytics and #content-strategy company"
created_at = "2017-06-28T20:31:12.391Z"
is_approved = true
job_board_url = "http://www.chimneygroup.com/join-us"
latitude = 52.5247484
longitude = 13.3990322
slug = "chimney"
tags = ["post-production", "production", "editorial", "sound-design", "analytics", "content-strategy"]
title = "Chimney"
updated_at = "2019-06-16T10:36:08.539Z"

+++
