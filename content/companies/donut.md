+++
job_board_url = "https://donut.recruitee.com"
title = "Donut"
slug = "donut"
body = "Investing for Outliers. Invest your spare change in Bitcoin and earn 8% with Crypto Savings."
tags = ["startup", "investing", "cryptocurrencies", "bitcoin"]
address = ""
postal_code = ""
city="berlin"
country="germany"
latitude = ""
longitude = ""
created_at = "2020-04-04T15:47:08.626Z"
updated_at = "2020-04-04T15:47:08.626Z"
company_url = "https://www.donut.app"
googleMapsUrl = ""
linkedin_url = "https://www.linkedin.com/company/trydonut/"
twitter_url = "https://twitter.com/donut__app"
is_approved = true
draft = false
+++
