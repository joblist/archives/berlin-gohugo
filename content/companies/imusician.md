+++
body = "online #music service for digital #music-distribution. Offers musicians services to sell, manage and monetise their music online; #publishing and administration of neighbouring rights; #legal"
created_at = "2018-02-03T19:09:49.445Z"
is_approved = true
job_board_url = "https://www.imusiciandigital.com/en/about-us/jobs/"
latitude = 52.503274
longitude = 13.4153698
slug = "imusician"
tags = ["music", "music-distribution", "publishing", "legal"]
title = "iMusician"
updated_at = "2019-06-16T10:36:08.538Z"

+++
