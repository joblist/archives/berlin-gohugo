+++
body = "#startup #fintech building a device that allows small merchants to accept card payments anywhere with a smartphone"
is_approved = true
job_board_url = "https://sumup.com/careers/positions/"
job_board_provider = "sumup"
job_board_hostname = "greenhouse"
latitude = 52.5197018
longitude = 13.4127558
slug = "sumup"
tags = ["startup", "fintech"]
title = "SumUp"
created_at = "2018-02-01T19:59:03.064Z"
updated_at = "2020-10-01T10:36:08.536Z"
+++
