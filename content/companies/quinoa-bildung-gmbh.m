+++
job_board_url = "https://www.quinoa-bildung.de/organisation/jobs"
job_board_provider = "smartrecruiters"
job_board_hostname = "QuinoaBildungGGmbH"
title = "Quinoa Bildung"
slug = "quinoa-bildung"
body = "School and youth formations"
tags = ["school", "teaching"]
address = "Kühnemannstrasse 26"
postal_code = "13409"
city="berlin"
country="germany"
latitude = 52.566119
longitude = 13.381054
company_url = "https://www.quinoa-bildung.de/"
facebook_url = "https://www.facebook.com/quinoabildung"
instagram_url = "https://www.instagram.com/quinoabildung"
created_at = "2020-07-04T23:10:18.626Z"
updated_at = "2020-07-04T23:10:18.626Z"
is_approved = true
draft = false
+++
