+++
body = "Unikrn Inc. - a company running the world's top regulated eSports betting platform with offices in Seattle, Las Vegas, Sydney, Germany and the Isle of Man. We heighten every gaming experience whether fans watch, play, or compete! #esport #bet #gaming"
created_at = "2020-02-24T10:43:44.425Z"
is_approved = true
job_board_url = "https://unikrn-jobs.personio.de/"
latitude = 52.542928
longitude = 13.422926
slug = "unikrn"
tags = ["esport", "bet", "gaming"]
title = "Unikrn"

+++
