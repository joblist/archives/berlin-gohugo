+++
body = "Business #travel made simple. Provides small and medium sized companies a truly unique solution. Travellers or travel managers can book and manage their business trips with one simple text message. #machine-learning #data #business-travel"
created_at = "2019-05-23T10:45:44.890Z"
is_approved = true
job_board_url = "https://comtravo.com/de/work-with-us/"
latitude = 52.49445
longitude = 13.4230699
slug = "comtravo"
tags = ["travel", "machine-learning", "data", "business-travel"]
title = "Comtravo"
updated_at = "2019-06-16T10:36:09.622Z"

+++
