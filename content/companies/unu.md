+++
body = "Creator of the unu electric scooter. #startup #electric"
created_at = "2018-02-09T15:53:37.653Z"
is_approved = true
job_board_url = "https://jobs.lever.co/unu"
latitude = 52.498407
longitude = 13.3835491
slug = "unu"
tags = ["startup", "electric"]
title = "Unu"
updated_at = "2019-06-16T10:36:09.730Z"

+++
