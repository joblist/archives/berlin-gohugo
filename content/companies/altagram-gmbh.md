+++
job_board_url = "https://altagram.com/careers"
job_board_provider = "smartrecruiters"
job_board_hostname = "AltagramGmbH"
title = "AltagramGmbH"
slug = "AltagramGmbH"
body = "Altagram is an independent localization agency based in Berlin. Contributes to the success of all types of video games (indie, mobile, AAA…), preparing them for international markets in over 50 languages."
tags = ["video-games", "localization", "agency", "translation", "languages"]
address = "Str. der Pariser Kommune 12-16"
postal_code = "10243"
city="berlin"
country="germany"
latitude = 52.510681
longitude = 13.438416
company_url = "https://altagram.com"
googlemaps_url = "https://g.page/AltagramBerlin"
linkedin_url = "https://twitter.com/Altagram_Group"
crunchbase_url = ""
twitter_url = "https://twitter.com/Altagram_Group"
facebook_url = "https://www.facebook.com/altagram.localization"
instagram_url = "https://www.instagram.com/altagram.localization"
created_at = "2018-02-09T07:12:52.566Z"
updated_at = "2020-07-04T22:26:18.626Z"
is_approved = true
draft = false
+++
