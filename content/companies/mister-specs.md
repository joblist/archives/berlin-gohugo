+++
job_board_url = "https://jobs.misterspex.com"
job_board_provider = "smartrecruiters"
job_board_hostname = "MisterSpexGmbH"
title = "Mister Spex"
slug = "mister-spex"
body = "We are establishing a new way of buying glasses that is in keeping with the times – one that combines the advantages of eCommerce with a range of services and advice from high street opticians. Greater choice and better, transparent prices in an easy, intuitive process. Put simply, with all the services our customers need. The way to buy eyewear today."
tags = ["startup", "eyewear", "e-commerce"]
address = "Greifswalder Str. 156"
postal_code = "10409"
city="berlin"
country="germany"
latitude = 52.543733
longitude = 13.441230
company_url = "https://misterspex.com"
linkedin_url = ""
twitter_url = "https://twitter.com/misterspex"
facebook_url = "https://www.facebook.com/misterspex"
instagram_url = "https://www.instagram.com/misterspex_official"
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
