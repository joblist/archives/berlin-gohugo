+++
body = "Travel Audience is the world's leading #data-driven travel #advertising #platform"
created_at = "2017-06-28T20:28:57.709Z"
is_approved = true
job_board_url = "https://travelaudience.com/careers"
latitude = 52.4897337
longitude = 13.4554343
slug = "travel-audience"
tags = ["data-driven", "advertising", "platform"]
title = "Travel Audience"
updated_at = "2019-06-16T10:36:09.731Z"

+++
