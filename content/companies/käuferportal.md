+++
body = "Käuferportal is Europe's market leader in connecting prospective online customers with specifically selected and verified companies. #startup"
created_at = "2018-02-08T09:26:33.174Z"
is_approved = true
job_board_url = "https://company.kaeuferportal.de/karriere/"
latitude = 52.5087788
longitude = 13.3756564
slug = "käuferportal"
tags = ["startup"]
title = "Käuferportal"
updated_at = "2019-06-16T10:36:09.744Z"

+++
