+++
body = "Video game #publisher focused on free to play #games; #software"
created_at = "2017-06-28T20:35:16.990Z"
is_approved = true
job_board_url = "http://corporate.aeriagames.com/careers/"
latitude = 52.4994456
longitude = 13.4492543
slug = "aeria-games"
tags = ["publisher", "games", "software"]
title = "Aeria Games"
updated_at = "2019-06-16T10:36:08.530Z"

+++
