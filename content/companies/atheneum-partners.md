+++
body = "Atheneum brings together the world’s leading professionals with the most qualified experts for experienced knowledge sharing and empowered decision-making. #expertise"
created_at = "2018-01-29T12:36:01.528Z"
is_approved = true
job_board_url = "https://atheneum.recruitee.com"
company_url = "https://www.atheneum.ai"
linkedin_url = "https://www.linkedin.com/company/atheneum-partners"
latitude = 52.5289797
longitude = 13.4100956
slug = "atheneum-partners"
tags = ["expertise"]
title = "Atheneum Partners"
updated_at = "2019-06-16T10:36:08.537Z"

+++
