+++
job_board_url = "https://dentolo-jobs.personio.de/"
title = "Dentolo"
body = "Dental insurance and ecosystem."
tags = ["insurance", "startup", "dental"]
address = "Schönhauser Allee 10-11"
postal_code = "10119"
city="berlin"
country="germany"
company_url = "https://www.dentolo.de"
slug = "dentolo-gmbh"
latitude = 52.530415
longitude = 13.411787
linkedin_url = ""
twitter_url = ""
instagram_url = ""
facebook_url = "https://www.facebook.com/dentolo.de"
created_at = "2020-09-08T16:55:18.626Z"
updated_at = "2020-09-08T16:55:18.626Z"
is_approved = true
draft = false
+++
