+++
body = "Our mission is to bring full CO2 transparency into the economy and enable companies to make the transition to climate neutrality.  #startup #ecology #climate"
created_at = "2020-03-01T16:25:05.750Z"
is_approved = true
job_board_url = "https://klimametrix.join.com/"
latitude = 52.500757728600306
longitude = 13.413276672363283
slug = "klimametrix"
tags = ["startup", "ecology", "climate"]
title = "Klima.Metrix"

+++
