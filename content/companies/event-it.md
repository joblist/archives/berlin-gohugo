+++
body = "#events organization for #it and #software"
created_at = "2018-02-02T10:47:18.396Z"
is_approved = true
job_board_url = "https://www.eventit.ag/jobs/"
latitude = 52.47942
longitude = 13.4317613
slug = "event-it"
tags = ["events", "it", "software"]
title = "Event it"
updated_at = "2019-06-16T10:36:08.536Z"

+++
