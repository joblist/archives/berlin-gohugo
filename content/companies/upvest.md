+++
job_board_url = "https://upvestco.recruitee.com"
title = "upvest"
slug = "upvest"
body = "Real estate asset tokenization made simple"
tags = ["startup", "blockchain", "real-estate"]
address = "Kleine Präsidentenstraße 1"
postal_code = "10178"
city="berlin"
country="germany"
latitude = 52.522346
longitude = 13.400306 
created_at = "2020-04-04T14:22:09.626Z"
updated_at = "2020-04-04T14:22:09.626Z"
company_url = "https://upvest.co"
linkedin_url = "https://www.linkedin.com/company/upvest"
twitter_url = "https://twitter.com/upvestco"
is_approved = true
draft = false
+++
