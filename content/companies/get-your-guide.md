+++
body = "GetYourGuide is a leading #travel #platform for #booking tours, attractions, excursions & #activities for your next #vacation or weekend adventure"
created_at = "2017-06-28T20:28:38.190Z"
is_approved = true
job_board_url = "http://careers.getyourguide.com"
latitude = 52.5486606
longitude = 13.4068523
slug = "get-your-guide"
tags = ["travel", "platform", "booking", "activities", "vacation"]
title = "Get Your Guide"
updated_at = "2019-06-16T10:36:08.541Z"

+++
