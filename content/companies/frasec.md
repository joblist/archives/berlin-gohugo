+++
body = "Frasec is a offers #job #security #service for the airport industry"
created_at = "2017-06-28T20:38:35.878Z"
is_approved = true
job_board_url = "https://www.frasec.de/stellenanzeigen/stellenangebote.html"
latitude = 52.3952284
longitude = 13.381913
slug = "frasec"
tags = ["job", "security", "service"]
title = "Frasec"
updated_at = "2019-06-16T10:36:09.728Z"

+++
