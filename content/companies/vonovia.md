+++
body = "Germany's largest residential property company. #real-estate #facility-management"
created_at = "2019-11-17T08:21:35.569Z"
is_approved = true
job_board_url = "https://karriere.vonovia.de/de-de/stellenboerse?Term=&Location=Berlin%2C+Germany&LocationLatitude=52.52000659999999&LocationLongitude=13.404953999999975&Radius=&Seniority=&Workspace="
slug = "vonovia"
tags = ["real-estate", "facility-management"]
title = "Vonovia"

+++
