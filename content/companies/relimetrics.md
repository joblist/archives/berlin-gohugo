+++
job_board_url = "https://relimetricsinc.recruitee.com"
title = "Relimetrics"
slug = "relimetrics"
body = "Relimetrics is part of the Industry 4.0 movement that is helping to transform how companies design and build products. Relimetrics uses computer vision and machine learning, and is a platform solution that is applicable to numerous industries, including automotive, manufacturing, plastics, insurance and others. Its software helps customers to build better quality products and increase productivity of their assembly or manufacturing processes."
tags = ["startup", "4.0", "design", "product", "computer-vision", "machine-learning", "platform"]
address = "Volmerstrasse 9B"
postal_code = "12489"
city="berlin"
country="germany"
latitude = 52.429374
longitude = 13.538042
created_at = "2020-04-04T14:06:09.626Z"
updated_at = "2020-04-04T14:06:09.626Z"
is_approved = true
draft = false
+++
