+++
body = "At kloeckner.i we follow the vision to digitalise the distribution of steel. To realize this vision we create digital solutions suited to our customers needs and demands, which serve to simplify transactions on all levels of the supply chain. #startup #industry"
created_at = "2018-02-14T06:55:29.433Z"
is_approved = true
job_board_url = "https://www.kloeckner-i.com/en/jobs/"
latitude = 52.53153
longitude = 13.3834999
slug = "kloeckneri"
tags = ["startup", "industry"]
title = "Kloeckner.i"
updated_at = "2019-06-16T10:36:09.735Z"

+++
