+++
body = "#ethical #marketplace and #co-operative business; products that are #fair to #nature and people"
created_at = "2017-06-28T20:23:35.756Z"
is_approved = true
job_board_url = "https://info.fairmondo.de/stellenausschreibungen/"
latitude = 52.4923667
longitude = 13.4367188
slug = "fairmondo"
tags = ["ethical", "marketplace", "co-operative", "fair", "nature"]
title = "Fairmondo"
updated_at = "2019-06-16T10:36:09.745Z"

+++
