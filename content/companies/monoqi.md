+++
body = "Monoqi is an online #platform dedicated to collect hard-to-find and limited-edition #design #objects"
created_at = "2017-06-28T20:36:24.961Z"
is_approved = true
job_board_url = "http://monoqi.com/de/jobs"
latitude = 52.5279347
longitude = 13.4047576
slug = "monoqi"
tags = ["platform", "design", "objects"]
title = "Monoqi"
updated_at = "2019-06-16T10:36:09.737Z"

+++
