+++
job_board_url = "https://www.parity.io/jobs"
title = "Parity"
body = " Blockchain Infrastructure for the Decentralised Web From the Substrate blockchain framework to Polkadot, the sharded protocol enabling blockchains to operate seamlessly together at scale, Parity builds the foundation of Web 3.0."
tags = ["blockchain", "software", "startup"]
address = ""
city="berlin"
country="germany"
company_url = "https://www.parity.io/jobs"
created_at = "2020-08-24T20:35:18.626Z"
updated_at = "2020-08-24T20:35:18.626Z"
is_approved = true
draft = false
+++
