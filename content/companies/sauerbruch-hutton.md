+++
body = "#agency for #architecture, #urbanism and #design. #interior-architecture #product-design #engineering #modelmaker"
created_at = "2018-02-02T10:43:12.282Z"
is_approved = true
job_board_url = "http://www.sauerbruchhutton.de/en/office#jobs"
latitude = 52.5325307
longitude = 13.3583072
slug = "sauerbruch-hutton"
tags = ["agency", "architecture", "urbanism", "design", "interior-architecture", "product-design", "engineering", "modelmaker"]
title = "Sauerbruch Hutton"
updated_at = "2019-06-16T10:36:08.532Z"

+++
