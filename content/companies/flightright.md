+++
body = "Judicial enforcement of compensation payments for air passengers #flight #airline #law"
created_at = "2018-02-01T16:44:44.600Z"
is_approved = true
job_board_url = "https://www.flightright.de/jobs"
latitude = 52.505687
longitude = 13.2992141
slug = "flightright"
tags = ["flight", "airline", "law"]
title = "Flightright"
updated_at = "2019-06-16T10:36:08.539Z"

+++
