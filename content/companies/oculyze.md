+++
body = "#technology #startup building mobile microscopy solutions based on recognition and #machine-learning platform; #bio-tech"
created_at = "2019-02-27T13:44:21.308Z"
is_approved = true
job_board_url = "https://www.oculyze.de/en/jobs/"
latitude = 52.3189251
longitude = 13.6289613
slug = "oculyze"
tags = ["technology", "startup", "machine-learning", "bio-tech"]
title = "Oculyze"
updated_at = "2019-06-16T10:36:09.623Z"

+++
