+++
title = "Searchmetrics"
slug = "searchmetrics"
body = "Searchmetrics' comprehensive suite of software and services delivers the data and insights that enable marketers to reach all of their search engine optimization and content marketing goals."
tags = ["marketing", "analytics", "startup"]
job_board_url = "https://searchmetrics.recruitee.com"
latitude =  52.538427
longitude = 13.435611
address = "Greifswalder Str. 212"
postal_code = "10405"
city="berlin"
country="germany"
created_at = "2020-04-04T13:34:09.626Z"
updated_at = "2020-04-04T12:54:09.626Z"
is_approved = true
+++
