+++
body = "#startup doing clothing size recommendations for online customers by measuring individual dimensions via webcams #ecommerce"
created_at = "2018-02-02T10:44:25.886Z"
is_approved = true
job_board_url = "https://boards.greenhouse.io/fitanalytics"
latitude = 52.514651
longitude = 13.467961
slug = "fit-analytics"
tags = ["startup", "ecommerce"]
title = "Fit Analytics"
updated_at = "2019-06-16T10:36:09.739Z"

+++
