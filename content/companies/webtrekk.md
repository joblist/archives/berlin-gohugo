+++
body = "Webtrekk is a #customer intelligence #platform that allows you to connect, analyze and activate your user and #marketing data across all devices"
created_at = "2017-06-28T20:28:46.515Z"
is_approved = true
job_board_url = "https://www.webtrekk.com/en/about/webtrekk-jobs/"
latitude = 52.5278235
longitude = 13.3795071
slug = "webtrekk"
tags = ["customer", "platform", "marketing"]
title = "Webtrekk"
updated_at = "2019-06-16T10:36:09.625Z"

+++
