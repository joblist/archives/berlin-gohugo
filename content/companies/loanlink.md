+++
title = "Loanlink"
slug = "loanlink"
body = "Loanlink is a fast growing startup reinventing the home buying experience in Germany by transforming an outdated and painful process with technology and innovation."
tags = ["startup", "buying", "technology", "innovation"]
job_board_url = "https://loanlinkgmbh.recruitee.com"
latitude = 52.539117
longitude = 13.411770 
address = " Schönhauser Allee 149"
postal_code = "10435"
city="berlin"
country="germany"
created_at = "2020-04-04T13:39:09.626Z"
updated_at = "2020-04-04T13:39:09.626Z"
is_approved = true
+++
