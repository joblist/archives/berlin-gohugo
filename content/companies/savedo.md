+++
body = "Savedo is an online #investment #platform allowing to buy attractive #financial products"
created_at = "2017-06-28T20:30:34.110Z"
is_approved = true
job_board_url = "https://www.savedo.de/ueber/karriere"
latitude = 52.5090673
longitude = 13.4211054
slug = "savedo"
tags = ["investment", "platform", "financial"]
title = "Savedo"
updated_at = "2019-06-16T10:36:08.535Z"

+++
