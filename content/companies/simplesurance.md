+++
body = "Simplesurance is a #platform for simple access to #insurance to combine traditional insurance industries with digital business."
created_at = "2017-06-28T20:35:33.853Z"
is_approved = true
job_board_url = "http://www.simplesurance-group.com/careers"
latitude = 52.4997318
longitude = 13.3814939
slug = "simplesurance"
tags = ["platform", "insurance"]
title = "Simplesurance"
updated_at = "2019-06-16T10:36:08.537Z"

+++
