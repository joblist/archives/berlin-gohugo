+++
job_board_url = "https://speedinvestgmbh.recruitee.com"
title = "Speedinvest"
slug = "speedinvest"
body = "Speedinvest is an early-stage venture capital fund with more than €400m to invest in visionary tech startups across Europe."
tags = ["venture-capital", "early-stage", "startup"]
address = "Dircksenstraße 47 "
postal_code = "10178"
city="berlin"
country="germany"
latitude = 52.523689
longitude = 13.404904
created_at = "2020-04-04T15:00:08.626Z"
updated_at = "2020-04-04T15:00:08.626Z"
company_url = "https://speedinvest.com/"
googleMapsUrl = "https://g.page/speedinvest-berlin"
is_approved = true
draft = false
+++
