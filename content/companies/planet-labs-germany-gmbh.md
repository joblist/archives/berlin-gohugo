+++
body = "Planet Labs is an imaging company that designs and manufactures miniature #satellites, equipped with #telescope and camera to capture Earth. The satellite constellation provides a complete image of Earth once per day at 3–5m optical resolution. The data provides information relevant to #climate monitoring, crop yield prediction, #urban-planning, and disaster response"
created_at = "2018-01-29T19:13:54.360Z"
is_approved = true
job_board_url = "https://www.planet.com/company/careers/?office=Berlin%2C%20Germany"
latitude = 52.5350925
longitude = 13.4135843
slug = "planet-labs-germany-gmbh"
tags = ["satellites", "telescope", "climate", "urban-planning"]
title = " Planet Labs Germany GmbH"
updated_at = "2019-06-16T10:36:09.730Z"

+++
