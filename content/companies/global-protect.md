+++
job_board_url = "https://www.global-protect.de/karriere"
job_board_provider = "smartrecruiters"
job_board_hostname = "HRDpt"
title = "Global Protect"
slug = "global-protect"
body = "Security services"
tags = ["security"]
address = "Regattastr. 189"
postal_code = "12527"
city="berlin"
country="germany"
latitude = 52.412671
longitude = 13.585590
company_url = "https://www.global-protect.de"
created_at = "2020-07-04T23:05:18.626Z"
updated_at = "2020-07-04T23:05:18.626Z"
is_approved = true
draft = false
+++
