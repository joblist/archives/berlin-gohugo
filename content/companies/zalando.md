+++
body = "Zalando is a German electronic #commerce company based in Berlin. The company maintains a #cross-platform online store that sells #shoes, #fashion and #beauty items"
created_at = "2017-06-28T20:25:12.586Z"
is_approved = true
job_board_url = "https://jobs.zalando.com/en/?location=Berlin"
slug = "zalando"
tags = ["commerce", "cross-platform", "shoes", "fashion", "beauty"]
title = "Zalando"
updated_at = "2019-06-16T10:36:09.637Z"

+++
