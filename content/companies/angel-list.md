+++
body = "General #job-board for #startup companies"
created_at = "2017-06-28T20:24:31.692Z"
is_approved = true
job_board_url = "https://angel.co/berlin/jobs"
slug = "angel-list"
tags = ["job-board", "startup"]
title = "Angel List"
updated_at = "2019-06-16T10:36:09.730Z"

+++
