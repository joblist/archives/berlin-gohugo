To create a new tag, replace the information in the template bellow to describe the tag you would like to submit.

Required (don't worry if you can't fill everything):
```
+++
title = "Startup"
+++
```

Optional:
```
+++
wikipedia_url = "https://en.wikipedia.org/wiki/Startup_company"
+++
```
