To create a new company, replace the information in the template bellow to describre the company you would like to submit.

`job_board_provider` and `job_board_hostname` are required for automatic job fetching.

Required (don't worry if you can't fill everything):
```
+++
job_board_url = "https://www.quinoa-bildung.de/organisation/jobs"
title = "Quinoa Bildung"
body = "School and youth formations"
tags = ["school", "teaching", "social-justice", "ecology", "climate-transition"]
address = "Kühnemannstrasse 26"
postal_code = "12033"
city="berlin"
country="germany"
company_url = "https://www.quinoa-bildung.de/"
+++
```

Optional:
```
slug = "quinoa-bildung"
job_board_provider = "greenhouse" // one of [greenhouse, recruitee, smartrecruiters]
job_board_hostname = "QuinoaBildungGGmbH" // the hostname, to access this company
latitude = 00.000000 // for a map position
longitude = 00.000000 // for a ma position
google_maps_url = "https://g.page/the-student-hotel-berlin" // to fetch google place data
linkedin_url = "https://linkedin.com/example"
facebook_url = "https://www.facebook.com/example"
instagram_url = "https://www.instagram.com/example"
created_at = "2020-07-04T23:10:18.626Z" // date added to the db
updated_at = "2020-07-04T23:10:18.626Z" // date last updated in db
```
